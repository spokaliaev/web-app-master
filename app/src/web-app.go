package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	version := os.Getenv("SOURCE_COMMIT")
	host, _ := os.Hostname()

	// TODO: Get rid of the code below
	// Skipping cert verification
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}

	fmt.Printf("[%s] Starting Web App...\n", time.Now())
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "!!! DEMO !!!\n")
		fmt.Fprint(w, "Web App:\n")
		fmt.Fprint(w, "\tVersion: ", version, "\n")
		fmt.Fprint(w, "\tHost: ", host, "\n")
		fmt.Fprint(w, "\tAPI Service URL: ", os.Args[2], "\n")
		fmt.Fprint(w, "API Service:\n")
		resp, err := client.Get(os.Args[2])
		if err != nil {
			fmt.Fprint(w, "\t", err.Error())
		} else {
			buf, _ := ioutil.ReadAll(resp.Body)
			var data map[string]interface{}
			json.Unmarshal(buf, &data)
			fmt.Fprint(w, "\tVersion: ", data["Version"], "\n")
			fmt.Fprint(w, "\tHost: ", data["Host"])
		}
	})

	fmt.Printf("Web App is listenning %s\n", os.Args[1])
	fmt.Printf("API Service URL:  %s\n", os.Args[2])
	log.Fatal(http.ListenAndServe(os.Args[1], nil))
}
