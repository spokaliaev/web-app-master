package e2e_tests

import (
	"testing"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"net/http"
	"crypto/tls"
	"os"
)

func TestCart(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "api service")
}

var _ = Describe("api service", func() {

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}


	Context("initially", func() {

		_, err := client.Get(os.Getenv("API_SERVICE_URL"))
		//_, err := client.Get("http://mail.ru")

		It("There is no error ", func() {
			Expect(err).Should(BeNil())
		})
	})

	Context("initially", func() {

		_, err := client.Get("")

		It("There is a error ", func() {
			Expect(err).ShouldNot(BeNil())
		})
	})
})